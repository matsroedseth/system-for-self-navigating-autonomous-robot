/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.robot;

/**
 * The IR class holds the parameters and characteristics of the IR sensor tower.
 * @author Thor Eivind and Mats (Master 2016 NTNU)
 */
public class IR {

    private int numberOfSensors;
    private int[] heading, spreading;

    public IR(int[] sensors) {
        numberOfSensors = sensors.length;
        spreading = new int[numberOfSensors];
        heading = sensors;
        
        for(int i=0;i<numberOfSensors;i++){
            spreading[i] = sensors[i]-sensors[0];
        }
    }

    public int getNumberOfSensors() {
        return numberOfSensors;
    }

    public void setNumberOfSensors(int numberOfSensors) {
        this.numberOfSensors = numberOfSensors;
    }

    public int[] getHeading() {
        return heading;
    }

    public int[] getSpreading() {
        return spreading;
    }
}

    