/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.robot;

/**
 * This class represents one measurement made by a robot.
 * It holds the robots pose and the ir sensor readings.
 * 
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class Measurement {
    private int xPos,yPos,theta;
    private int[] IRdata, IRheading;

    Measurement(int measuredOrientation, int[] measuredPosition, int[] irHeading, int[] irData) {
        this.theta = measuredOrientation;
        this.xPos = measuredPosition[0];
        this.yPos = measuredPosition[1];
        this.IRheading = irHeading;
        this.IRdata = irData;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public int getTheta() {
        return theta;
    }
    
    public int[] getIRHeading(){
        return IRheading;
    }

    public int[] getIRdata() {
        return IRdata;
    }
}