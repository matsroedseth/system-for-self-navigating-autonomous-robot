/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.robot;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * This class represents each robot. It holds the robots parameters and
 * characteristics. The class holds on two different objects, IR and a
 * concurrentLinkedQueue of Measurements.
 *
 * The concurrentLinkedQueue "measurements" holds all Measurements done by the robot, that is not yet processed by the program.
 * 
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class Robot {

    private final int id;
    private final String name;
    private final int width, length, axleOffset;
    private final int messageDeadline;
    private final int[] towerOffset, sensorOffset;
    private final IR irSensors;
    private final ConcurrentLinkedQueue<Measurement> measurments;

    private int[] estimatedPosition;
    private int robotOrientation;

    /**
     * Second constructor of the class Robot
     *
     * @param robotID The robots ID
     * @param name The robots name
     * @param width The robots physical width (cm)
     * @param length The robots physical length (cm)
     * @param messageDeadline The robots message deadline (min update rate[ms])
     * @param axleOffset Axle offset lengthwise
     * @param towerOffset Tower offset [a,b] a-lengthwise, b-crosswise
     * @param sensorOffset Sensor offset [a,b,c,d,..] from centre (radius)
     * @param irHeading IR heading relative to 0 deg (straight forward)
     */
    public Robot(int robotID, String name, int width, int length, int messageDeadline,
            int axleOffset, int[] towerOffset, int[] sensorOffset, int[] irHeading) {
        this.id = robotID;
        this.name = name;
        this.width = width;
        this.length = length;
        this.messageDeadline = messageDeadline;
        this.axleOffset = axleOffset;
        this.towerOffset = towerOffset;
        this.sensorOffset = sensorOffset;
        this.irSensors = new IR(irHeading);
        this.measurments = new ConcurrentLinkedQueue<>();
    }

    /**
     * Puts a Measurement at the end of the measurement queue. This method is
     * thread safe and will never block. It can throw a NullPointerExeption if
     * some of the values are wrong or null, if not it will return true
     *
     * @param measuredOrientation Measured theta
     * @param measuredPosition Measured position as an int[] x first, then y
     * @param towerHeading the heading of the first sensor in the ir tower
     * @param irData the ir data that where taken at the same time.
     * @return true if sucessfull, it may trow an exeption if some parameters
     * are wrong or null
     */
    public boolean addMeasurement(int measuredOrientation, int[] measuredPosition, int towerHeading, int[] irData) {
        int[] irHeading = new int[irData.length];
        for (int i = 0; i < irData.length; i++) {
            irHeading[i] = (towerHeading + irSensors.getSpreading()[i]) % 360;
        }

        Measurement measurment = new Measurement(measuredOrientation, measuredPosition, irHeading, irData);
        return measurments.offer(measurment);
    }

    /**
     * Returns and removes the oldest measurement done by the robot, this method
     * is thread safe and will return null if there are no more measurements
     * left.
     *
     * @return the oldest measurement done by this robot or null if there are no
     * measurements in the queue.
     */
    public Measurement getMeasurement() {
        return measurments.poll();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int[] getPosition() {
        return estimatedPosition;
    }

    public void setPosition(int[] position) {
        this.estimatedPosition = position;
        System.out.println("I'm " + this.name + " and my position is: x:" + this.estimatedPosition[0] + ",y:" + this.estimatedPosition[1]);
    }

    public int getRobotOrientation() {
        return robotOrientation;
    }

    public void setRobotOrientation(int robotOrientation) {
        this.robotOrientation = robotOrientation;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getMessageDeadline() {
        return messageDeadline;
    }

    public int[] getTowerOffset() {
        return towerOffset;
    }

    public int[] getSensorOffset() {
        return sensorOffset;
    }

    public int getAxleOffset() {
        return axleOffset;
    }

    public IR getIRSensors() {
        return irSensors;
    }
}