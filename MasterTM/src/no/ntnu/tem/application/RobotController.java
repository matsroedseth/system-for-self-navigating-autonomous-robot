/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.application;

import no.ntnu.tem.robot.Robot;
import java.util.ArrayList;
// må ha bool på at en robot er oppdatert

/**
 * RobotController holds a List over all robots in the system. It holds methods
 * for adding and removing robots as well as searching for a special robot.
 *
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class RobotController {

    private final ArrayList<Robot> robotList;

    /**
     * Constructor of the class RobotController, starts off with an empty list
     * of robots.
     */
    public RobotController() {
        robotList = new ArrayList<Robot>();
    }

    public ArrayList<Robot> getRobotList() {
        return robotList;
    }

    /**
     * Method that adds a robot to the system
     *
     * @param robotID The robots id
     * @param name The robots name
     * @param width The robots physical width (cm)
     * @param length The robots physical length (cm)
     * @param messageDeadline The robots message deadline (min update rate[ms])
     * @param axleOffset Axle offset lengthwise
     * @param towerOffset Tower offset [a,b] a-lengthwise, b-crosswise
     * @param sensorOffset Sensor offset [a,b,c,d,..] from centre (radius)
     * @param irHeading IR heading relative to 0 deg (straight forward)
     * @return true if successful
     */
    public boolean addRobot(int robotID, String name, int width, int length, int messageDeadline,
            int axleOffset, int[] towerOffset, int[] sensorOffset, int[] irHeading) {
        if (getRobot(robotID) == null) {
            try {
                Robot robot = new Robot(robotID, name, width, length, messageDeadline,
                        axleOffset, towerOffset, sensorOffset, irHeading);
                robotList.add(robot);
                System.out.println("Robot <" + robot.getName() + "> created!");
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    /**
     * Method that adds a measurement to a robot
     * @param robotName The robots name
     * @param measuredOrientation The robots measured orientation
     * @param measuredPosition The robots measured position
     * @param towerHeading The robots tower angle
     * @param irData The measured IR-data
     * @return 
     */
    public boolean addMeasurment(String robotName, int measuredOrientation, int[] measuredPosition, int towerHeading, int[] irData) {
        Robot robot = getRobot(robotName);
        if (robot == null) {
            return false;
        }
        return robot.addMeasurement(measuredOrientation, measuredPosition, towerHeading, irData);
    }

    /**
     * Method that returns the robot, if existing, with the given name
     *
     * @param name the robot name
     * @return the first robot with the given name
     */
    public Robot getRobot(String name) {
        for (Robot r : robotList) {
            if (r.getName().equalsIgnoreCase(name)) {
                return r;
            }
        }
        return null;
    }

    /**
     * Method that returns the robot, if existing, with the given robot id
     *
     * @param robotID the robot id
     * @return the first robot with the given id
     */
    public Robot getRobot(int robotID) {
        for (Robot r : robotList) {
            if (r.getId() == robotID) {
                return r;
            }
        }
        return null;
    }

    /**
     * Method that removes the robot, if existing, with the given name
     *
     * @param name the robot to be removed
     * @return true if successful
     */
    public boolean removeRobot(String name) {
        for (Robot r : robotList) {
            if (r.getName().equalsIgnoreCase(name)) {
                robotList.remove(r);
                return true;
            }
        }
        return false;
    }
}
