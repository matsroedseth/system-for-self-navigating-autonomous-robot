/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.application;

import java.util.Iterator;
import java.util.LinkedList;
import no.ntnu.tem.communication.Communication;
import no.ntnu.tem.communication.ListPorts;
import no.ntnu.tem.simulator.Simulator;

/**
 *
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class Application {

    private final Communication com;
    private final RobotController rc;
    private final ListPorts lp;
    private LinkedList<String> portList;

    private boolean simulatorActive = true;

    /**
     * Constructor of the class Application
     */
    public Application() {
        
        lp = new ListPorts();
        this.portList = lp.listPorts();
        for(int i = 0;i<portList.size();i++){
            System.out.println("Her: "+portList.get(i));
        }
        
        rc = new RobotController();
        com = new Communication(rc);
        if (!simulatorActive) {
            com.startCommunication();
        } else {
            String[] robotNames = {"Mats", "TE", "Eirik"};
            String mapLocation = "C:\\Users\\matsgr\\Documents\\NetBeans\\MasterTM\\src\\no\\ntnu\\tem\\simulator\\maps\\map.txt";
            //new Simulator(mapLocation, robotNames, com.getInbox())
                    //.start();
        }
        com.startInboxReader();
    }
    
    public LinkedList<String> getPortList(){
        return portList;
    }

//    public void writeCommandToRobot(String robotName, double orientation, double distance){
//        if(simulatorActive){
//            sim.sendCommandToVirtualRobot(robotName, orientation, distance);
//        }else{
//            com.sendMessageToRobot(robotName, orientation, distance);
//        }
//    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Application();
    }

}
