/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.DoubleToIntFunction;
import javax.swing.JFrame;
import javax.swing.Timer;
import no.ntnu.tem.communication.Inbox;

/**
 * This class contains both the interface and the control system of the
 * simulator. This is the main class in the simulator package.
 *
 * @author Eirik Thon
 */

public class Simulator {

    private Timer updateDisplay;
    private final SimWorld world;
    private GUI gui;
    private boolean estimateNoiseEnabled;
    private boolean sensorNoiseEnabled;
    ArrayList<RobotHandler> robotHandlers;
    Inbox inbox;

    /**
     * Constructor. Creates an instance of the Simulator class with a number of
     * robots given by the length of "robotNames" and map from the file
     * specified by "mapPath"
     *
     * @param robotNames String[]
     * @param mapPath String
     */
    public Simulator(String mapPath, String[] robotNames, Inbox inbox) {
        this.inbox = inbox;
        world = new SimWorld();
        world.initMap(mapPath);
        robotHandlers = new ArrayList<RobotHandler>();
        createRobots(robotNames);
        estimateNoiseEnabled = true;
        sensorNoiseEnabled = true;

    }

    /**
     * Creates and adds robots to the SimWorld. The parameter "names" specifies
     * the names of the robots
     *
     * @param String[]
     */
    private void createRobots(String[] names) {
        Random r = new Random();
        ArrayList<Feature> allFeatures = world.getFeaturesCopy();
        Position initialPosition = new Position();
        int numberOfRobots = names.length;
        for (int i = 0; i < numberOfRobots; i++) {
            int heigth = world.getMapHeight();
            int width = world.getMapWidth();

            // Create the initial position avoiding collision with any other
            // object
            do {
                double initialPosX = Math.abs(r.nextInt()) % heigth;
                double initialPosY = Math.abs(r.nextInt()) % width;
                initialPosition = new Position(initialPosX, initialPosY);
            } while (Utilities.isCollision(initialPosition, allFeatures));
            Angle initialHeading = new Angle(Math.abs(r.nextInt()) % 360);
            RobotPose initialPose = new RobotPose(initialPosition, initialHeading);

            // Make sure no robots end up on top of each other
            Feature robotFeature = initialPose.pose2Feature(initialHeading);
            allFeatures.add(robotFeature);

            // Create the robot
            String name = names[i];
            Robot newRobot = new Robot(initialPose, name, i);

            //Dummy Handshake
            int robotWidth = 40;
            int robotLength = 60;
            int toweroffset[] = {30, 40};
            int axleoffset = 0;
            int[] sensoroffset = {5, 5, 5, 5};
            int[] irheading = {0, 90, 180, 270};
            int messageDeadline = 400;
            String robotHandshake = generateHandshake(robotWidth, robotLength, toweroffset, axleoffset, sensoroffset, irheading, messageDeadline);
            String dongleSim = "[" + i + "]:" + names[i] + ":";
            String handshake = dongleSim + robotHandshake;
            inbox.putMessage(handshake);
            world.addRobot(newRobot);

            //Dummy Update
            int[] sensors = {0, 0, 0, 0};
            String robotUpdate = generateUpdate(((Double) (initialPosition.xValue)).intValue(), ((Double) (initialPosition.yValue)).intValue(), ((Double) (initialHeading.value)).intValue(), 0, sensors);
            String update = dongleSim + robotUpdate;
            inbox.putMessage(update);

            // Create and start the robot handler
            RobotHandler robotHandler = this.new RobotHandler(newRobot);
            robotHandlers.add(robotHandler);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // Create and run an instance if the Simulator class
        String mapPath = "C:\\Users\\teanders\\Documents\\masterproject\\MasterTM\\src\\no\\ntnu\\tem\\simulator\\maps\\map.txt";
        int numberOfRobots = 20;
        String[] names = new String[numberOfRobots];
        for (int i = 0; i < numberOfRobots; i++) {
            names[i] = Integer.toString(i);
        }
        Simulator simulator = new Simulator(mapPath, names, new Inbox());
        simulator.start();

        // Give robots random targets
        Random r = new Random();
        int i = 0;
        while (true) {
            if (i > 19) {
                i = 0;
            }
            double dist = Math.abs(r.nextDouble()) * 200;
            double theta = Math.abs(r.nextDouble()) * 360 - 180;
            String name = Integer.toString(i);
            simulator.setRobotCommand(name, theta, dist);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
        //Simulator.stop();
    }

    /**
     * Opens the GUI and starts the simulator
     */
    public void start() {
        for (RobotHandler robotHandler : robotHandlers) {
            robotHandler.start();
        }
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                initializeGUI();
            }
        });
    }

    /**
     * Stops the simulator, and closes the window
     */
    public void stop() {
        // Uses the closing routine defined in initializeGUI
        gui.dispatchEvent(new WindowEvent(gui, WindowEvent.WINDOW_CLOSING));
    }

    /**
     * Turns estimation error on/off
     */
    void flipEstimateNoiseEnabled() {
        estimateNoiseEnabled = !estimateNoiseEnabled;
    }

    /**
     * Turns sensor error on/off
     */
    void flipSensorNoiseEnabled() {
        sensorNoiseEnabled = !sensorNoiseEnabled;
    }

    /**
     * Gives the command ("rotation", "angle") to the robot with name "name"
     *
     * @param name String
     * @param rotation double
     * @param distance double
     */
    public void setRobotCommand(String name, double rotation, double distance) {
        world.getRobot(name).setTarget(rotation, distance);
    }

    /**
     * Initializes the graphical user interface
     */
    private void initializeGUI() {
        gui = new GUI(this, world);
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DisplayHandler displayHandler = this.new DisplayHandler();
        updateDisplay = new Timer(10, displayHandler);
        updateDisplay.start();
        gui.setTitle("Simulator");
        gui.setVisible(true);

        // Define closing routine
        gui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                updateDisplay.stop();
                for (RobotHandler robotHandler : robotHandlers) {
                    robotHandler.interrupt();
                }
            }
        });
    }

    private static String generateHandshake(int robotWidth, int robotLength, int[] toweroffset, int axleoffset, int[] sensoroffset, int[] irheading, int messageDeadline) {
        // 02 is handshake
        return "{02," + robotWidth + "," + robotLength + "," + toweroffset[0] + "," + toweroffset[1] + "," + axleoffset + "," + sensoroffset[0] + "," + sensoroffset[1] + "," + sensoroffset[2] + "," + sensoroffset[3] + "," + irheading[0] + "," + irheading[1] + "," + irheading[2] + "," + irheading[3] + "," + messageDeadline + "," + 999 + "}\n";
    }

    private static String generateUpdate(int xPos, int yPos, int robotHeading, int towerHeading, int[] sensor) {
        // 01 is update
        return "{01," + xPos + "," + yPos + "," + robotHeading + "," + towerHeading + "," + sensor[0] + "," + sensor[1] + "," + sensor[2] + "," + sensor[3] + ",0}\n";
    }

    /**
     * This object repaints the world object every 10 ms, thereby creating the
     * animation shown in the GUI
     */
    private class DisplayHandler implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            world.repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException ex) {
                System.out.println("Display interrupted");
            }
        }
    }

    private class RobotHandler extends Thread {

        /**
         * Controller object for a robot. This class contains a Robot object and
         * is responsible for making that robot run.
         */
        final private Robot myRobot;
        final private String myName;
        private double estimateNoise;
        private double sensorNoise;
        private final Random noiseGenerator;

        /**
         * Constructor
         *
         * @param robot Robot
         */
        public RobotHandler(Robot robot) {
            myRobot = robot;
            myName = robot.getName();
            noiseGenerator = new Random();
        }

        /**
         * A continuous loop that calls methods of the robot in a specific way
         * in order to generate the robot behavior.
         */
        @Override
        public void run() {
            int counter = 0;
            myRobot.shakeHands();
            while (true) {
                // Get all map features
                ArrayList<Feature> allFeatures = world.getFeaturesCopy();

                // Create features from other robot positions and add them to
                // allFeatures
                ArrayList<String> names = world.getAllRobotNamesCopy();
                RobotPose myPose;
                myPose = myRobot.getPoseCopy();
                for (String name : names) {
                    if (name == myName) {
                        continue;
                    }
                    RobotPose otherPose;
                    otherPose = world.getRobot(name).getPoseCopy();
                    Angle angleBetweenRobots = new Angle(Math.toDegrees(Math.atan2(otherPose.position.yValue - myPose.position.yValue, otherPose.position.xValue - myPose.position.xValue)));
                    Feature robotFeature = otherPose.pose2Feature(angleBetweenRobots);
                    allFeatures.add(robotFeature);
                }

                // Move robot
                if (estimateNoiseEnabled) {
                    estimateNoise = noiseGenerator.nextGaussian() * 0.1;
                } else {
                    estimateNoise = 0;
                }
                myRobot.moveRobot(allFeatures, estimateNoise);

                // Measure
                if (counter > 19) { // Every 200 ms ( Counter is increased every 10 ms )
                    myRobot.turnTower();
                    if (sensorNoiseEnabled) {
                        sensorNoise = noiseGenerator.nextGaussian();
                    } else {
                        sensorNoise = 0;
                    }
                    myRobot.measureIR(allFeatures, sensorNoise);
                    myRobot.transmitAllSensorData();
                    String dongleSim = "[" + 0 + "]:"+myName+":";
                    String update = generateUpdate(20, 30, 200, 0, new int[] {0,0,0,0});
//                    System.out.println(System.nanoTime());
                    inbox.putMessage(dongleSim+update);
                    
                    counter = 0;
                }
                counter++;

                // Pause
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    break;
                }
            }
        }
    }
}
