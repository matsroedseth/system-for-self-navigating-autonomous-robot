/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import javax.swing.JComponent;
import static no.ntnu.tem.simulator.Utilities.polar2cart;

/**
 * This class represents the environment of the simulator. It contains all the
 * features and all the robots. It extends the JCoponent and has a paint method
 * that allows this object to be displayed in an interface
 * @author Eirik Thon
 */
public class SimWorld extends JComponent{
    private int width;
    private int height;
    ArrayList<Feature> features;
    HashMap<String,Robot> robots;
    private Object robotLock = new Object();
    private Object nameLock = new Object();
    private Object featureLock = new Object();
    private Object measurementsLock = new Object();
    private boolean showMeasurements;
    private boolean showTarget;
    private boolean showEstimatedPose;
    ArrayList<String> robotNames;

    /**
     * Default constructor
     */
    public SimWorld() {
        width = 0;
        height = 0;
        features = new ArrayList<Feature>(0);
        robots = new HashMap<String, Robot>(0);
        showMeasurements = true;
        showTarget = false;
        showEstimatedPose = false;
        robotNames = new ArrayList<String> (0);
    }
    
    /**
     * Returns the height of the map
     * @return height
     */
    int getMapHeight() {
        return height;
    }
    
    /**
     * Returns the width of the map
     * @return width
     */
    int getMapWidth() {
        return width;
    }
    
    /**
     * Adds the robot given by the input parameter to the SimWorls. Safe to call
     * concurrently
     * @param robot 
     */
    void addRobot(Robot robot) {
        String name = robot.getName();
        synchronized(nameLock) {
            robotNames.add(name);
        }
        synchronized(robotLock) {
            robots.put(name, robot);
        }
    }
    
    /**
    * Creates and returns a copy of the features in the SimWorld.
    * This function is safe to call concurrently.
    * @return ArrayList<String> robotNamesCopy
    */
    ArrayList<Feature> getFeaturesCopy() {
        synchronized(featureLock) {
            ArrayList<Feature> featuresCopy = new ArrayList<Feature>(0);
            for (Feature feature : features) {
                featuresCopy.add(new Feature(feature));
            }
            return featuresCopy;
        }    
    }
    
    /**
     * Returns the Robot object with the name matching the input parameter.
     * This function is safe to call concurrently.
     * @param name
     * @return Robot
     */
    Robot getRobot(String name) {
        synchronized(robotLock) {
            return robots.get(name);
        }
    }
    
    /**
     * Switches an internal state in the SimWorld so that the robots
     * measurements are being painted or stops being painted
     */
    void flipShowMeasurements() {
        showMeasurements = !showMeasurements;
    }
    
    /**
     * Switches an internal state in the SimWorld so that the robots target
     * position are being painted or stops being painted
     */
    void flipShowTarget() {
        showTarget = !showTarget;
    }
    
    /**
     * Switches an internal state in the SimWorld so that the robots estimated
     * poses are being painted or stops being painted
     */
    void flipShowEstimatedPose() {
        showEstimatedPose = !showEstimatedPose;
    }
    
    /**
     * Creates and returns a copy of the robot names stored in the SimWorld.
     * This function is safe to call concurrently.
     * @return ArrayList<String> robotNamesCopy
     */
    ArrayList<String> getAllRobotNamesCopy() {
        ArrayList<String> robotNamesCopy = new ArrayList<String> (0);
        synchronized(nameLock) {
            for (String name : robotNames) {
                String copy = name;
                robotNamesCopy.add(copy);
            }
        }
        return robotNamesCopy;
    }
    
    /**
     * Sets the map size and adds all features in the map based upon data found
     * in the text file given by the input parameter filename
     * @param filename The string filename specifies the map file
     */
    void initMap(String filename){
        try{
            File map = new File(filename);
            Scanner sc = new Scanner(map);
            
            initWorldSize(sc.next());
            while(sc.hasNext()){
                String featurePositionString = sc.next();
                String[] stringParts = featurePositionString.split(":");
                Position start = Utilities.string2Position(stringParts[0]);
                Position end = Utilities.string2Position(stringParts[1]);
                Feature newFeature = new Feature(start, end);
                features.add(newFeature);
            }
            sc.close();
            addBorderFeatures();
        }
        catch(FileNotFoundException e){
            System.out.println("File not found");
        }        
    }
    
    /**
     * Reads the contents of the input parameter size and updates the width
     * and height of the world based upon this
     * @param size Sting on the format "width:height"
     */
    private void initWorldSize(String size){
        String[] stringParts = size.split(":");
        width = Integer.parseInt(stringParts[0]);
        height = Integer.parseInt(stringParts[1]);
        setSize(width,height);
    }
    
    /**
     * Creates features that lies along the edges of the map and adds them to
     * the other features
     */
    private void addBorderFeatures(){
        Feature leftBorder = new Feature(0,0,0,height);
        features.add(leftBorder);
        Feature rightBorder = new Feature(width,0,width,height);
        features.add(rightBorder);
        Feature topBorder = new Feature(0,0,width,0);
        features.add(topBorder);
        Feature bottomBorder = new Feature(0,height,width,height);
        features.add(bottomBorder);
    }
    
    /**
     * Draws onto the parameter g all the features, robot poses,
     * and if selected in the GUI, measurements, target positions 
     * and estimated positions
     * @param g Swing Graphics object
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;

        // Save the initial transform
        AffineTransform initial = g2D.getTransform();
        
        // Turn the graphic object so that the origin is in the bottom left corner instead of the top left corner
        g2D.translate(0, getHeight() - 1);
        g2D.scale(1, -1);
        
        //Paint features
        g2D.setPaint(Color.black);
        for (Feature feature: features)
            feature.paintFeature(g2D);
        
        
        ArrayList<String> allRobotNames = getAllRobotNamesCopy();
        for (String name: allRobotNames) {
            // Select robot color
            int id;
            synchronized(robotLock) {
                id = robots.get(name).getId();
            }
            Color color = Utilities.selectColor(g2D, id);
            g2D.setPaint(color);
            // Paint robot estimate
            if(showEstimatedPose) {
                float alpha = 0.75f;
                int type = AlphaComposite.SRC_OVER; 
                AlphaComposite composite = AlphaComposite.getInstance(type, alpha);
                RobotPose estimatedPose;
                synchronized(robotLock) {
                    estimatedPose = robots.get(name).getEstimatedPoseCopy();
                }
                estimatedPose.paintPose(g2D);
            }
            
            // Paint robot
            RobotPose pose;
            synchronized(robotLock) {
                pose = robots.get(name).getPoseCopy();
            }
            
            pose.paintPose(g2D);
            // Paint target            
            if (showTarget) {
                Position target;
                synchronized(robotLock) {
                    target = robots.get(name).targetPosition;;
                }
                target.drawCross(g2D, 3);
            }

            // Paint measurement history
            if (showMeasurements) {
                LinkedList<double[]> measurementHistory;
                synchronized(robotLock) {
                    measurementHistory = robots.get(name).getMeasurementHistoryCopy();
                }
                for (double[] allMeasurements : measurementHistory) {
                    double xOrigin = allMeasurements[0];
                    double yOrigin = allMeasurements[1];
                    for (int i = 4; i < 8; i++) {
                        if (allMeasurements[i] != -1) {
                            double[] positionFormOrigin = polar2cart(allMeasurements[3]+(90*i), allMeasurements[i]);
                            double xValue = xOrigin + positionFormOrigin[0];
                            double yValue = yOrigin + positionFormOrigin[1];
                            Position measurement = new Position(xValue, yValue);
                            measurement.drawCross(g2D, 3);
                        }
                    }
                }
            }
        }
        // Return the old transform to paint the image "upside down"
        g2D.setTransform(initial);
    }
}
