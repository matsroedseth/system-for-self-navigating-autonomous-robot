/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.simulator;

import java.util.LinkedList;
import no.ntnu.tem.communication.Inbox;

/**
 *
 * @author matsroedseth
 */
public class OldSimulator {
    private final Inbox inbox;
    private final LinkedList robotList;
    
    
    public OldSimulator (Inbox inbox){
        this.inbox = inbox;
        robotList = new LinkedList();
    }
    
    private void putMessageIntoInbox(/*JSONMelding fra virtuell robot*/){
        //inbox.putMessage(/*JSONObject (melding)*/);
    }
    
    public void sendCommandToVirtualRobot(String robotName, double orientation, double distance){
        /*
         * Ta inputen
         * Bruke MessageHandler.wrapMessage(robotName, orientation, distance)
            * Denne metoden returnerer JSON
         * Videresend melding til Virtuell Robot
        */
        //f.eks:
        //simcom.send(MessageHandler.wrapMessage(robotName, orientation, distance));
    }
}
