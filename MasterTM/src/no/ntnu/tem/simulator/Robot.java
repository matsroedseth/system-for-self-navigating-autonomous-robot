/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import static no.ntnu.tem.simulator.Utilities.polar2cart;

/**
 * This class represents the virtual robots in the simulator. It has private
 * variables to represent the state of the robots such as the pose, and store
 * data such as the measurements. The robots actions are created by calling
 * methods in this class such as moveRobot().
 * @author Eirik Thon
 */
public class Robot {
    private RobotPose pose;
    private RobotPose estimatedPose;
    private String name;
    private int id;
    private double measuredDistance;
    private double measuredRotation;
    private double targetDistance;
    private double targetRotation;
    private int rotationDirection;
    private boolean rotationFinished;
    private boolean translationFinished;
    private double towerAngle;
    final private double turnSpeed = 0.3;
    final private double moveSpeed = 0.3;
    private int towerDirection;
    final private Object measurementLock = new Object();
    final private Object movementLock = new Object();
    final private double maxVisualLength = 60;
    final private double minVisualLength = 10;
    private double[] lastIrMeasurement;
    private LinkedList<double[]> measurementHistory;
    Position targetPosition;
    
    /**
     * Constructor for Robot.
     * @param initialPose
     * @param name
     * @param id 
     */
    public Robot(RobotPose initialPose, String name, int id) {
        towerDirection = 1;
        towerAngle = 0;
        this.name = name;
        this.id = id;
        pose = initialPose;
        estimatedPose = new RobotPose(initialPose);
        targetDistance = 0;
        targetRotation = 0;
        measuredDistance = 0;
        measuredRotation = 0;
        measurementHistory = new LinkedList<double[]>();
        lastIrMeasurement = new double[4];
        rotationDirection = 1;
        rotationFinished = true;
        translationFinished = true;       
        targetPosition = new Position(pose.position.xValue,pose.position.yValue);
    }
    
    /**
     * Returns the ID of the robot
     * @return integer
     */
    int getId() {
        return id;
    }
    
    /**
     * Returns the name of the robot
     * @return String
     */
    String getName() {
        return name;
    }
    
    /**
     * Sets the target rotation and target heading. Also resets the measured
     * rotation and distance.
     * @param theta
     * @param distance 
     */
    void setTarget(double theta, double distance) {
        synchronized(movementLock) {
            targetRotation = theta;
            targetDistance = distance;
            measuredRotation = 0;
            measuredDistance = 0;
            rotationDirection = (int)Math.signum(theta);
            double[] offset = polar2cart(theta+pose.heading.value,distance);
            targetPosition = new Position(pose.position.xValue + offset[0],pose.position.yValue + offset[1]);
            if (targetRotation != 0) {
                rotationFinished = false;
            }
            if (targetDistance != 0) {
                translationFinished = false;
            }
        }
    }
    
    /**
     * Creates and returns a copy of the pose
     * @return 
     */
    RobotPose getPoseCopy() {
        return new RobotPose(pose);
    }
    
    /**
     * Creates and returns a copy of the estimated pose
     * @return 
     */
    RobotPose getEstimatedPoseCopy() {
        return new RobotPose(estimatedPose);
    }
    
    /**
     * Turns the tower 5 degrees within a 90 degree angle.
     */
    void turnTower() {
        if (towerAngle == 0 && towerDirection < 0) {
            towerDirection = 1;
        } else if (towerAngle == 90 && towerDirection > 0) {
            towerDirection = -1;
        }
        towerAngle += towerDirection * 5;
    }
    
    /**
     * Creates a package of all measurements with a timestamp. The package is 
     * sent to the application and also stored in the measurement history of the
     * robot.
     */
    void transmitAllSensorData() {
        double[] measurement = new double[9];
        measurement[0] = estimatedPose.position.xValue;
        measurement[1] = estimatedPose.position.yValue;
        measurement[2] = estimatedPose.heading.value;
        measurement[3] = towerAngle + estimatedPose.heading.value;
        measurement[8] = (double)System.nanoTime();
        for (int i = 4; i < 8; i++) {
            measurement[i] = lastIrMeasurement[i-4];
        }
        synchronized (measurementLock){
            measurementHistory.add(measurement);
            if (measurementHistory.size() > 5) {
                measurementHistory.remove();
            }
        }
        //inbox.
    }

    /**
     * Rotate or move the robot a little distance towards the target position.
     * The robot will not move if the movement leads to a collision between
     * any of the features in "allFeatures". If the destination is reached
     * the robot will not move. The estimated pose is also updated and the 
     * value of "noise" is added to the estimate.
     * @param allFeatures ArrayList<Feature>
     * @param noise double
     */
    void moveRobot(ArrayList<Feature> allFeatures, double noise) {
        synchronized(movementLock) {
            if (!rotationFinished) {
                pose.heading.add(turnSpeed*rotationDirection);
                estimatedPose.heading.add(turnSpeed*rotationDirection+noise);
                measuredRotation += turnSpeed*rotationDirection+noise;
                if (Math.abs(measuredRotation) >= Math.abs(targetRotation)){
                    measuredRotation = 0;
                    rotationFinished = true;
                }
            } else if (!translationFinished) {
                Position positionCopy = new Position(pose.position);
                positionCopy.moveInDirection(pose.heading.value, moveSpeed);
                if (!Utilities.isCollision(positionCopy, allFeatures)) {
                    pose.position.moveInDirection(pose.heading.value, moveSpeed);
                    estimatedPose.position.moveInDirection(estimatedPose.heading.value, moveSpeed + noise);
                    measuredDistance += moveSpeed+noise;
                    if(measuredDistance >= targetDistance) {
                        measuredDistance = 0;
                        translationFinished = true;
                    }
                }
            }
        }
    }
    
    /**
     * Compute the intersection between each sensors line of sight and any of
     * the features, and add the distance to the intersection as a new
     * measurement. The value given in "noise" is added to the measurement
     * @param features ArrayList<Feature>
     * @param sensorNoise double
     */
    void measureIR(ArrayList<Feature> features, double sensorNoise) {
        for (int i = 0; i < 4; i++) {
            lastIrMeasurement[i] = -1;
            double shortestMeasurement = maxVisualLength;
            for (Feature feature : features) {
                // Convert sensor line of sight to a Feature
                Position lineOfSightStart = new Position(pose.position);
                double[] tempCartesian = Utilities.polar2cart(towerAngle + pose.heading.value + i * 90, maxVisualLength);
                Position lineOfSightEnd = new Position(pose.position.xValue + tempCartesian[0], pose.position.yValue + tempCartesian[1]);
                Feature lineOfSight = new Feature(lineOfSightStart, lineOfSightEnd);

                // Compute measurement as an intersection point between the current Feature and line of sight
                double newIRMeasurement = Utilities.getDistanceToIntersection(lineOfSight, feature);
                
                // Add only the measurement of the nearest feature
                if (newIRMeasurement != -1 && newIRMeasurement < shortestMeasurement) {
                    shortestMeasurement = newIRMeasurement;
                    lastIrMeasurement[i] = newIRMeasurement + sensorNoise;
                }
                
                // Discard the measurement if it is within minVisualLength
                if (lastIrMeasurement[i] < minVisualLength) {
                    lastIrMeasurement[i] = -1;
                }
            }
        }
    }
    
    /**
     * Creates and returns a copy of the robots measurementHistory
     * @return 
     */
    LinkedList<double[]> getMeasurementHistoryCopy() {
        LinkedList<double[]> measurementHistoryCopy = new LinkedList<double[]> ();
        synchronized(measurementLock) {
            for (double[] measurement: measurementHistory) {
                measurementHistoryCopy.add(new double[9]);
                for (int i = 0; i < 9; i++) {
                    measurementHistoryCopy.getLast()[i] = measurement[i];
                }
            }
        }
        return measurementHistoryCopy;
    }
    
    void shakeHands() {
        
    }
}
