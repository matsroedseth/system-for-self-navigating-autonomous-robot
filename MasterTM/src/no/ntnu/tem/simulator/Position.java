/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;
import java.awt.Graphics2D;

/**
 * This class represents a position using an x-value, an a y-value
 * @author Eirik Thon
 */
public class Position {
    double xValue;
    double yValue;
    
    /**
     * Constructor. Initializes to zero
     */
    public Position() {
        xValue = 0;
        yValue = 0;
    }
    
    /**
     * Constructor
     * @param xPosition double
     * @param yPosition double
     */
    public Position(double xPosition, double yPosition) {
        this.xValue = xPosition;
        this.yValue = yPosition;
    }
    
    /**
     * Copy constructor. Creates and returns a copy of the Position
     * @param otherPosition 
     */
    public Position(Position otherPosition) {
        this(otherPosition.xValue, otherPosition.yValue);
    }
 
    /**
     * Moves the position the given distance in the give direction
     * @param direction double
     * @param distance double
     */
    void moveInDirection(double direction, double distance){
        double[] tempTarget = Utilities.polar2cart(direction, distance);
        xValue = xValue + tempTarget[0];
        yValue = yValue + tempTarget[1];
    }
    
    /**
     * Computes the distance to another position
     * @param other
     * @return double
     */
    double distanceTo(Position other) {
        return Math.sqrt(Math.pow(xValue - other.xValue, 2) + Math.pow(yValue - other.yValue, 2));
    }
    
    /**
     * writes the values to System.out
     */
    void print() {
        System.out.println("x-position " + xValue + ", y-position " + yValue);
    }
    
    /**
     * Draws onto "g2D" a circle centered at the position.
     * @param g2D Graphics2D
     * @param diameter double
     */
    void drawCircle(Graphics2D g2D, int diameter) {
        g2D.drawOval((int)Math.round(xValue)-diameter/2,(int)Math.round(yValue)-diameter/2, diameter, diameter);
    }
    
    /**
     * Draws onto "g2D" a cross centered at the position. Size determines the
     * distance from the center of the cross to the end points
     * @param g2D Graphics2D
     * @param size integer
     */
    void drawCross(Graphics2D g2D, int size) {
        g2D.drawLine((int)Math.round(xValue-size), (int)Math.round(yValue-size),
                                (int)Math.round(xValue+size), (int)Math.round(yValue+size));
        g2D.drawLine((int)Math.round(xValue-size), (int)Math.round(yValue+size),
                (int)Math.round(xValue+size), (int)Math.round(yValue-size));
    }
    
    /**
     * NOT USED
     * @param other 
     */
    void add(Position other) {
        xValue += other.xValue;
        yValue += other.yValue;
    }
}
