/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.awt.Graphics2D;
import static no.ntnu.tem.simulator.Utilities.polar2cart;

/**
 * This class is used to represent angles. The angle is represented as a double
 * in the range [0,360). All functions in this class are implemented so that the 
 * value stays within these bounds.
 * @author eirith
 */
public class Angle {
    double value;

    public Angle() {
        value = 0;
    }
    
    /**
     * Copy constructor.
     * Creates a copy of otherAngle
     * @param otherAngle 
     */
    public Angle(Angle otherAngle) {
        // Copy constructor
        this(otherAngle.value);
    }
    
    /**
     * Constructor
     * @param angle value of initial angle
     */
    public Angle(double angle) {
        this.value = angle % 360;
        if (value < 0) {
            value += 360;
        }
    }
    
    /**
     * Returns a new angle with the value of the angle plus addedAngle. The
     * the angle stays unchanged
     * @param addedAngle
     * @return 
     */
    Angle add(Angle addedAngle){
        Angle returnAngle = new Angle();
        if (value + addedAngle.value % 360 >= 360)
            returnAngle.value = value - 360 + addedAngle.value % 360;
        else
            returnAngle.value = value + addedAngle.value % 360;
        return returnAngle;
    }
    
    /**
     * Adds the value addedAngle to the angle
     * @param addedAngle 
     */
    void add(double addedAngle){
        if (value + addedAngle % 360 >= 360)
            value = value - 360 + addedAngle % 360;
        else
            value = value + addedAngle % 360;
    } 

    /**
     * Prints the value of the angle
     */
    void print() {
        System.out.println("Angle: " + value);
    }
    
    /**
     * Draws a line onto g2D starting in "start", with length "length" in the
     * direction of the angle
     * @param g2D
     * @param start
     * @param length 
     */
    void drawAngle (Graphics2D g2D, Position start, int length) {
        double[] headingPoint = polar2cart((double) value, length);
        g2D.drawLine((int)Math.round(start.xValue), (int)Math.round(start.yValue),
                (int)Math.round(start.xValue+headingPoint[0]), (int)Math.round(start.yValue+headingPoint[1]));
    }
}
