/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.awt.Graphics2D;


/**
 * This class represent a wall or obstacle in the map. The feature must be a
 * straight line, and it is parameterized by a start and end position.
 * @author Eirik Thon
 */
public class Feature {
    Position start;
    Position end;
    
    /**
     * Constructor
     * @param start Position
     * @param end Position
     */
    public Feature(Position start, Position end) {
        this.start = start;
        this.end = end;
    }
    
    /**
     * Copy constructor. Creates a copy of otherFeature
     * @param otherFeature 
     */
    public Feature(Feature otherFeature) {
        this(otherFeature.start, otherFeature.end);
    }
    
    /**
     * Constructor
     * @param xStart
     * @param yStart
     * @param xEnd
     * @param yEnd 
     */
    public Feature(double xStart, double yStart, double xEnd, double yEnd) {
        this.start = new Position(xStart, yStart);
        this.end = new Position(xEnd, yEnd);
    }
    
    /**
     * Prints the position of the feature using System.out
     */
    void print(){
        start.print();
        end.print();
    }
    
    /**
     * Paints a line between the start and end position of the feature onto
     * "g2D".
     * @param g2D Graphics2D
     */
    void paintFeature(Graphics2D g2D) {
       // Draw line between start and end
       g2D.drawLine((int)Math.round(start.xValue), (int)Math.round(start.yValue),
                   (int)Math.round(end.xValue), (int)Math.round(end.yValue));        
    }
}
