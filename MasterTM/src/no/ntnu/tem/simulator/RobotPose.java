/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;
import java.awt.Graphics2D;
import static no.ntnu.tem.simulator.Utilities.polar2cart;

/**
 * This class is used to represent the pose of a robot. The pose consists of
 * the position and orientation of the robot.
 * @author eirith
 */
public class RobotPose {
    Position position;
    Angle heading;
    
    /**
     * Constructor
     */
    public RobotPose(){
        position = new Position();
        heading = new Angle();
    }
    
    /**
     * Initializing constructor
     * @param initialPosition
     * @param initialHeading 
     */
    public RobotPose(Position initialPosition, Angle initialHeading){
        position = initialPosition;
        heading = initialHeading;
    }
    
    /**
     * Copy constructor. A deep copy of the original object is created
     * @param otherPose RobotPose
     */    
    public RobotPose(RobotPose otherPose){
        // Copy constructor
        this(new Position(otherPose.position), new Angle(otherPose.heading));
    }
    
    /**
     * Paints the pose onto "g2D"
     * @param g2D Graphics2D
     */
    void paintPose(Graphics2D g2D){
        position.drawCircle(g2D, 10);
        heading.drawAngle(g2D, position, 8);
    }
    
    /**
     * Creates and returns a new feature centered at the poses position, with
     * length 10 and normal to the angle "newFeatureDirection"
     * @param newFeatureDirection Angle
     * @return 
     */
    Feature pose2Feature (Angle newFeatureDirection) {
        Angle startAngle = newFeatureDirection.add(new Angle(90));
        Angle endAngle = newFeatureDirection.add(new Angle(-90));
        double[] startCoordinates = polar2cart(startAngle.value, 5);
        double[] endCoordinates = polar2cart(endAngle.value, 5);
        Position start = new Position(position.xValue + startCoordinates[0], position.yValue + startCoordinates[1]);
        Position end = new Position(position.xValue + endCoordinates[0], position.yValue + endCoordinates[1]);
        return new Feature(start, end);
    }
}
