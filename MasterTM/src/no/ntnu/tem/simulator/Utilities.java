/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package no.ntnu.tem.simulator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class contains static methods useful to other classes.
 * @author eirith
 */
public class Utilities {
    
    /**
     * converts from polar to Cartesian coordinates.
     * @param theta double
     * @param r double
     * @return double[2], [0] = x-value, [1] = y-value
     */
    static double[] polar2cart(double theta, double r){
        double thetaRad = Math.toRadians(theta);
        double x = r*Math.cos(thetaRad);
        double y = r*Math.sin(thetaRad);
        double[] cart = {x,y};
        return cart;
    }
    
    /**
     * Creates a new Position from data in "string". "string" should be on the
     * format "x,y".
     * @param string
     * @return 
     */
    static Position string2Position(String string){
        String[] stringParts = string.split(",");
        double x = Integer.parseInt(stringParts[0]);
        double y = Integer.parseInt(stringParts[1]);
        Position position = new Position(x,y);
        return position;
    }
    
    /**
     * Creates a unit vector along the feature in the direction from start to
     * to the end point of the feature.
     * @param feature Feature
     * @return double[2]
     */
    static double[] feature2Vector(Feature feature) {
        double x = feature.end.xValue-feature.start.xValue;
        double y = feature.end.yValue-feature.start.yValue;
        double distance = feature.start.distanceTo(feature.end);
        double[] vector = {x/distance, y/distance};
        return vector;
    }
    
    /**
     * Computes and returns the end position of a vector. The vector is
     * parameterized by a starting point, unit vector, and a distance along
     * the unit vector
     * @param start Position
     * @param unitVector double[2]
     * @param distance double
     * @return 
     */
    static Position vectorFindEnd(Position start, double[] unitVector, double distance) {
        Position end = new Position();
        end.xValue = start.xValue + unitVector[0]*distance;
        end.yValue = start.yValue + unitVector[1]*distance;
        return end;
    }
    
    /**
     * Checks if "feature1" and "feature2" intersects by calculation. If an
     * intersection is found the method returns the distance from the start of 
     * "feature1" to the intersection. Otherwise -1 is returned.
     * @param feature1 Feature
     * @param feature2 Feature
     * @return double
     */
    static double getDistanceToIntersection(Feature feature1, Feature feature2) {
        double[] v1 = feature2Vector(feature1);
        double[] v2 = feature2Vector(feature2);
        double dx = feature2.start.xValue - feature1.start.xValue;
        double dy = feature2.start.yValue - feature1.start.yValue;
        double div = v2[0]*v1[1] - v2[1]*v1[0];
        if (div == 0) {
            return -1;
        }
        double u = (dy * v2[0] - dx * v2[1]) / div;
        double v = (dy * v1[0] - dx * v1[1]) / div;
        
        // Check if an intersection was found and if it is within the endpoints of the features
        if (u > 0 && v > 0 && u < feature1.start.distanceTo(feature1.end)&& v < feature2.start.distanceTo(feature2.end)) {
            return u;
        }
        else
            return -1;
    }
    
    /**
     * Returns true if the distance from "center" to any of the features in
     * "features" is less than 5. Otherwise returns false.
     * @param center Position
     * @param features ArrayList<Feature>
     * @return boolean
     */
    static boolean isCollision(Position center, ArrayList<Feature> features) {
        double collisionSize = 5;
        for (Feature feature: features) {
            // Create vector from feature start to position
            double aVectorX = center.xValue - feature.start.xValue;
            double aVectorY = center.yValue - feature.start.yValue;
            double[] aVector = {aVectorX, aVectorY};
            
            // Create unitvector from feature
            double[] bVector = feature2Vector(feature);
            
            // Project a onto b
            double projectionLength = aVector[0]*bVector[0] + aVector[1]*bVector[1];
            
            if (projectionLength > feature.start.distanceTo(feature.end) || projectionLength < 0) {
                continue;
            }
            
            // Find endpoint of projection
            Position projectionEnd = vectorFindEnd(feature.start, bVector, projectionLength);
            
            // Check for collision
            if (center.distanceTo(projectionEnd) < collisionSize) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Sets the color of "g" to a color within a predefined set of colors. The
     * parameter "i" is used to select a color from the set. Modulo is used if
     * i is larger than the size of the set
     * @param g Graphics2D
     * @param i integer
     * @return Color
     */
    static Color selectColor(Graphics2D g, int i) {
        switch (i % 10) {
            case 0:
                return Color.blue;
            case 1:
                return Color.red;
            case 2:
                return Color.green;
            case 3:
                return Color.pink;
            case 4:
                return Color.orange;
            case 5:
                return Color.cyan;
            case 6:
                return Color.pink;
            case 7:  
                return Color.magenta;
            case 8:
                return Color.darkGray;
            case 9:
                return Color.lightGray;
            default:
                return Color.black;
        }
    }
}
