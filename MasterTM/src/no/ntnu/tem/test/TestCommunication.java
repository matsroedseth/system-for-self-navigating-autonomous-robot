/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.test;

import no.ntnu.tem.application.RobotController;
import no.ntnu.tem.communication.SerialCommunication;
import no.ntnu.tem.communication.Inbox;
import no.ntnu.tem.communication.InboxReader;

/**
 *
 * @author matsroedseth
 */
public class TestCommunication {
    
    
    
    public TestCommunication(){
        Inbox inbox = new Inbox();
        RobotController rc = new RobotController();
        InboxReader inR = new InboxReader(inbox, rc);
        SerialCommunication com = new SerialCommunication(inbox);
        com.start();
        inR.start();
    }
    
    public static void main(String[] args){
        TestCommunication test = new TestCommunication();
    }
}
