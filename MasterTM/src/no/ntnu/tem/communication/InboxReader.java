/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.communication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minidev.json.JSONObject;
import no.ntnu.tem.application.RobotController;
import no.ntnu.tem.robot.Robot;

/**
 * This class retrieves the messages from the inbox and interprets them using
 * the class MessageHandler. Furthermore it pushes the information to the
 * RobotController and thus to the rest of the application.
 *
 * @author Thor Eivind and Mats (Master 2016 at NTNU)
 */
public class InboxReader extends Thread {

    private RobotController rc;
    private Inbox inbox;
    private static final int update = 1;
    private static final int handshake = 2;
    private static final int response = 3;

    private HashMap<Integer, String> messageList;

    /**
     * Constructor of the class InboxReader
     *
     * @param inbox the systems Inbox
     * @param rc the systems RobotController
     */
    public InboxReader(Inbox inbox, RobotController rc) {
        this.rc = rc;
        this.inbox = inbox;
        this.messageList = new HashMap<Integer, String>();
    }

    /**
     * Hente og tolke melding fra inbox
     */
    private void readMessage() {
        if (inbox.getInboxSize() > 0) {
            String message = inbox.getMessage();
            // Decode message
            int robotID = Integer.parseInt(message.substring(1, 2));
            //Append to correct id
            //Check if content is complete
            String name = message.split(":")[1];
            String tmpContent = message.split(":")[2];

            String content = messageMerger(robotID, tmpContent);

            if (content == null) {

                return; //incomplete message,
            }
            //Messagehandler
            //System.out.println("Hello, i'm " + name + ", i'm number " + robotID + " and my content is: ");

            interpretContent(name, robotID, content);
        }
    }

    private void interpretContent(String name, int robotID, String content) {
        try {
            content = content.replace("{", "");
            content = content.replace("}", "");
            content = content.replace("\n", "");

            int[] irHeading;
            int messagetype = MessageHandler.getMessageType(content);
            int checksum = 0;

            switch (messagetype) {
                case handshake:
                    System.out.println("CASE: HANDSHAKE");
                    int width = MessageHandler.getRobotWidth(content);
                    int length = MessageHandler.getRobotLength(content);
                    int axleOffset = MessageHandler.getAxleOffset(content);
                    int messageDeadline = MessageHandler.getMessageDeadline(content);
                    checksum = MessageHandler.getMessageChecksum(content);
                    int[] towerOffset = MessageHandler.getTowerOffset(content);
                    int[] sensorOffset = MessageHandler.getSensorOffset(content);
                    irHeading = MessageHandler.getRobotIRHeading(content);

//                    System.out.print("Messagetype: " + messagetype + ", irheading: ");
//                    for (int j = 0; j < irHeading.length; j++) {
//                        System.out.print(irHeading[j] + ", ");
//                    }
//                    System.out.print(", Length: " + length + ", Width: " + width
//                    + ", AxleOffset: " + axleOffset + ", TowerOffset: ");
//                    for (int j = 0; j < towerOffset.length; j++) {
//                        System.out.print(towerOffset[j] + ", ");
//                    }
//                    System.out.print("SensorOffset: ");
//                    for (int j = 0; j < sensorOffset.length; j++) {
//                        System.out.print(sensorOffset[j] + ", ");
//                    }
//                    System.out.println("MessageDeadline: " + messageDeadline + ", Checksum: " + checksum);
                    doHandshake(robotID, name, width, length, axleOffset, messageDeadline, towerOffset, sensorOffset, irHeading);

                    break;

                case (update): //UPDATE
                    System.out.println("CASE: UPDATE");
                    int orientation = MessageHandler.getOrientationData(content);
                    int[] position = MessageHandler.getRobotPositionData(content);
                    int towerHeading = MessageHandler.getRobotTowerHeading(content);
                    int[] irData = MessageHandler.getRobotIRData(content);
                    checksum = MessageHandler.getMessageChecksum(content);
                    doUpdate(robotID, name, orientation, position, towerHeading, irData);

//                    System.out.print("Messagetype: " + messagetype + ", Position: ");
//                    for (int j = 0; j < position.length; j++) {
//                        System.out.print(position[j] + ", ");
//                    }
//                    System.out.print("IRHeading: ");
//                    for (int j = 0; j < irHeading.length; j++) {
//                        System.out.print(irHeading[j] + ", ");
//                    }
//                    System.out.print("IRData:");
//                    for (int j = 0; j < irData.length; j++) {
//                        System.out.print(irData[j] + ", ");
//                    }
//                    System.out.println("Checksum: "+checksum);
                    break;

            }

        } catch (MessageHandler.MessageCorruptException ex) {
            Logger.getLogger(InboxReader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private String findMessageByRobotID(int id) {
        if (messageList.containsKey(id)) {
            return messageList.get(id);
        } else {
            return "";
        }
    }

    private String messageMerger(int robotID, String content) {
        String mergedContent = findMessageByRobotID(robotID) + content;
        if (mergedContent.startsWith("{") && mergedContent.endsWith("\n")) {
            messageList.put(robotID, "");
            return mergedContent;
        } else {
            messageList.put(robotID, mergedContent);
            return null;
        }
    }

    @Override
    public void run() {
        super.run(); //To change body of generated methods, choose Tools | Templates.
        while (true) {
            readMessage();
        }
    }

    /**
     * Method for adding a new robot to the system after receiving a handshake
     *
     * @param name The robots name
     * @param width The robots physical width (cm)
     * @param length The robots physical length (cm)
     * @param axleOffset Axle offset lengthwise
     * @param messageDeadline The robots message deadline (min update rate[ms])
     * @param towerOffset Tower offset [a,b] a-lengthwise, b-crosswise
     * @param sensorOffset Sensor offset [a,b,c,d,..] from centre (radius)
     * @param irHeading IR heading relative to 0 deg (straight forward)
     */
    private void doHandshake(int robotID, String name, int width, int length,
            int axleOffset, int messageDeadline, int[] towerOffset, int[] sensorOffset,
            int[] irHeading) {
        rc.addRobot(robotID, name, width, length, messageDeadline, axleOffset,
                towerOffset, sensorOffset, irHeading);
    }

    /**
     * Method for updating one of the robots in the system
     *
     * @param name The robots name
     * @param orientation The robots orientation
     * @param position The robots position (x,y)
     * @param irHeading The IR-sensors heading
     * @param irData Data gathered from IR-sensors
     */
    private void doUpdate(int robotID, String name, int orientation, int[] position,
            int towerHeading, int[] irData) {
        rc.addMeasurment(name, orientation, position, towerHeading, irData);
    }

}

//
///**
//     * Method that uses MessageHandler to interpret incoming messages
//     */
//    private void readMessageOld() {
//
//        try {
//            if (inbox.getInboxSize() > 0) {
//                JSONObject message = inbox.getMessageOld();
//
//                double orientation = 0;
//                double[] position = new double[]{0};
//                double[] irHeading = new double[]{0};
//                int[] irData = new int[]{0};
//                String name = MessageHandler.getRobotName(message);
//
//                switch (MessageHandler.getMessageType(message)) {
//
//                    case (update): //UPDATE
//
//                        System.out.println("CASE: UPDATE");
//                        boolean gotIRSensors = MessageHandler.gotIRData(message);
//                        boolean gotPosition = MessageHandler.gotPosition(message);
//                        boolean gotOrientation = MessageHandler.gotOrientation(message);
//                        if (gotOrientation) {
//                            orientation = MessageHandler.getOrientationData(message);
//                        }
//                        if (gotPosition) {
//                            position = MessageHandler.getRobotPositionData(message);
//                        }
//                        if (gotIRSensors) {
//                            irHeading = MessageHandler.getRobotIRHeading(message);
//                            irData = MessageHandler.getRobotIRData(message);
//                        }
//                        if (gotOrientation && gotPosition && gotIRSensors) {
//                            doUpdate(name, orientation, position, irHeading, irData);
//                        }
//
//                        break;
//                    case (handshake): //HANDSHAKE
//
//                        System.out.println("CASE: HANDSHAKE");
//                        double width = MessageHandler.getRobotWidth(message);
//                        double length = MessageHandler.getRobotLength(message);
//                        double axleOffset = MessageHandler.getAxleOffset(message);
//                        int messageDeadline = MessageHandler.getMessageDeadline(message);
//                        double[] towerOffset = MessageHandler.getTowerOffset(message);
//                        double[] sensorOffset = MessageHandler.getSensorOffset(message);
//                        irHeading = MessageHandler.getRobotIRHeading(message);
//
//                        doHandshake(name, width, length, axleOffset, messageDeadline,
//                                towerOffset, sensorOffset, irHeading);
//                        break;
//                    case (response):
//                        System.out.println("CASE: RESPONSE");
//
//                }
//            } else {
//                try {
//                    Thread.sleep(10);
//                } catch (InterruptedException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        } catch (MessageHandler.MessageCorruptException mce) {
//            mce.printStackTrace();
//        }
//    }
