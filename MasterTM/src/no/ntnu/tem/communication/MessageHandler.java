/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.communication;

import java.util.Arrays;
//import net.minidev.json.JSONObject;

/**
 *
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class MessageHandler implements Language {

    /**
     * Method that returns message type (handshake/update)
     *
     * @param str message to interpret
     * @return message type (handshake or update)
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if str is not structured properly
     */
    public static int getMessageType(String str) throws MessageCorruptException {
        try {
            return (int) Integer.parseInt(str.split(",")[MESSAGETYPE]);
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the IR tower angle, used only during update, if you
     * are using it during handshake, use getRobotIRHeading(handshake) instead
     *
     * @param update message to interpret
     * @return the angle of the IR tower
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if update is not structured properly
     */
    public static int getRobotTowerHeading(String update) throws MessageCorruptException {
        try {
            String[] content = update.split(",");
            return Integer.parseInt(content[UPDATE_TOWER_HEADING]);
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the IR headings, used only during handshake, if you
     * are using it during update use getRobotTowerHeading(update) instead
     *
     * @param handshake message to interpret
     * @return an array of all IR headings, can be used to calculate ir
     * spreading
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int[] getRobotIRHeading(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int[] data = new int[NUMBEROFSENSORS];
            content = Arrays.copyOfRange(content, 10, 14);
            for (int i = 0; i < content.length; i++) {
                data[i] = Integer.parseInt(content[i]);
            }
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the data gathered by the ir-sensors. Used only during
     * update.
     *
     * @param update message to interpret
     * @return array containing the sensor data
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if update is not structured properly
     */
    public static int[] getRobotIRData(String update) throws MessageCorruptException {
        try {
            String[] content = update.split(",");
            int[] data = new int[NUMBEROFSENSORS];
            content = Arrays.copyOfRange(content, UPDATE_SENSOR_START, UPDATE_SENSOR_END + 1);
            for (int i = 0; i < content.length; i++) {
                data[i] = Integer.parseInt(content[i]);
            }
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the robots position. Used only during Update
     *
     * @param update message to interpret
     * @return array containing the x and y coordinate of the robot
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if update is not structured properly
     */
    public static int[] getRobotPositionData(String update) throws MessageCorruptException {
        try {
            String[] content = update.split(",");
            return new int[]{Integer.parseInt(content[UPDATE_XPOS]), Integer.parseInt(content[UPDATE_YPOS])};
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the robots orientation. Used only during Update
     *
     * @param update message to interpret
     * @return the robots orientation
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if update is not structured properly
     */
    public static int getOrientationData(String update) throws MessageCorruptException {
        try {
            String[] content = update.split(",");
            int data = Integer.parseInt(content[UPDATE_THETA]);
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the robots physical width. Used only during Handshake
     *
     * @param handshake message to interpret
     * @return the robot width
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int getRobotWidth(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int data = Integer.parseInt(content[HANDSHAKE_WIDTH]);
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the robots physical length. Used only during
     * Handshake
     *
     * @param handshake message to interpret
     * @return the robot length
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int getRobotLength(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int data = Integer.parseInt(content[HANDSHAKE_LENGTH]);
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the tower offset [a,b]: a-lengthwise, b-crosswise.
     * Used during Handshake.
     *
     * @param handshake message to interpret
     * @return array containing the tower offset
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int[] getTowerOffset(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            return new int[]{Integer.parseInt(content[HANDSHAKE_TOWEROFFSET_Y]), Integer.parseInt(content[HANDSHAKE_TOWEROFFSET_X])};
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the axle offset lengthwise. Used during Handshake
     *
     * @param handshake message to interpret
     * @return the axle offset
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int getAxleOffset(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int data = Integer.parseInt(content[HANDSHAKE_AXLEOFFSET]);
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }

    }

    /**
     * Method that returns the sensor offset from center of the tower (radius).
     * Used during Handshake
     *
     * @param handshake message to interpret
     * @return array containing the sensor offset
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int[] getSensorOffset(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int[] data = new int[NUMBEROFSENSORS];
            content = Arrays.copyOfRange(content, HANDSHAKE_SENSOROFFSET_START, HANDSHAKE_SENSOROFFSET_END + 1);
            for (int i = 0; i < content.length; i++) {
                data[i] = Integer.parseInt(content[i]);
            }
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the robots message deadline. Used during handshake
     *
     * @param handshake message to interpret
     * @return the message deadline (ms)
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if handshake is not structured properly
     */
    public static int getMessageDeadline(String handshake) throws MessageCorruptException {
        try {
            String[] content = handshake.split(",");
            int data = Integer.parseInt(content[HANDSHAKE_MESSAGEDEADLINE]);
            return data;
        } catch (Exception e) {
            throw new MessageCorruptException();
        }
    }

    /**
     * Method that returns the message checksum
     *
     * @param str message to interpret
     * @return the message checksum
     * @throws no.ntnu.tem.communication.MessageHandler.MessageCorruptException
     * if str is not structured properly
     */
    public static int getMessageChecksum(String str) throws MessageCorruptException {
        String[] content = str.split(",");
        int data = Integer.parseInt(content[content.length - 1]);
        return data;
    }

    /**
     * Method that wraps content into a message
     *
     * @param robotName the robot name
     * @param orientation the robots orientation
     * @param distance the robots distance to travel
     * @return wrapped message
     */
    public static String wrapMessage(String robotName, double orientation, double distance) {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append(robotName);
        sb.append(",");
        sb.append(orientation);
        sb.append(",");
        sb.append(distance);
        sb.append("}");
        String message = sb.toString();
        return message;
    }

    /**
     * Exception thrown when the message is corrupt
     */
    public static class MessageCorruptException extends Exception {

        public MessageCorruptException() {
        }
    }
}

/////////////////////JSON MESSAGES/////////////////////
/*
public static boolean gotPosition(JSONObject json) {
        return json.containsKey(POSITION);
    }
    public static int getMessageDeadline(JSONObject json) throws MessageCorruptException {
        return (int) json.get(MESSAGEDEADLINE);
    }
    public static double[] getSensorOffset(JSONObject json) throws MessageCorruptException {
        return (double[]) json.get(IRSENSOROFFSET);
    }
    public static double getAxleOffset(JSONObject json) throws MessageCorruptException {
        return (double) json.get(AXLEOFFSET);
    }
    public static double[] getTowerOffset(JSONObject json) throws MessageCorruptException {
        return (double[]) json.get(TOWEROFFSET);
    }
    public static double getRobotLength(JSONObject json) throws MessageCorruptException {
        return (double) json.get(ROBOTLENGTH);
    }
    public static double getRobotWidth(JSONObject json) throws MessageCorruptException {
        return (double) json.get(ROBOTWIDTH);
    }
    public static double getOrientationData(JSONObject json) throws MessageCorruptException {
        return (double) json.get(ORIENTATION);
    }
        public static double[] getRobotPositionData(JSONObject json) throws MessageCorruptException {
        JSONObject acc = (JSONObject) json.get(POSITION);
        double[] data = new double[acc.size()];
        for (int j = 0; j < acc.size(); j++) {
            data[j] = (int) acc.get(POSITIONCOORDINATES + (j + 1));
        }
        return data;
    }
    public static int[] getRobotIRData(JSONObject json) throws MessageCorruptException {
        JSONObject ir = (JSONObject) json.get(IRDATA);
        int[] data = new int[ir.size()];
        for (int j = 0; j < ir.size(); j++) {
            data[j] = (int) ir.get(IRSENSOR + (j + 1));
        }
        return data;
    }
    public static double[] getRobotIRHeading(JSONObject json) throws MessageCorruptException {
        JSONObject ir = (JSONObject) json.get(IRHEADING);
        double[] data = new double[ir.size()];
        for (int j = 0; j < ir.size(); j++) {
            data[j] = (int) ir.get(IRSENSOR + (j + 1));
        }
        return data;
    }
    public static int getMessageType(JSONObject json) throws MessageCorruptException {
        return (int) json.get(MESSAGETYPE);
    }

    public static String getRobotName(JSONObject json) throws MessageCorruptException {
        return (String) json.get(ROBOTNAME);
    }
    
    public static boolean gotOrientation(JSONObject json) {
        return json.containsKey(ORIENTATION);
    }
    
    public static boolean gotIRData(JSONObject json) {
        return json.containsKey(IRDATA);
    }
    public static JSONObject wrapMessage(String robotName, double orientation, double distance) {
        JSONObject message = new JSONObject();
        message.put(ROBOTNAME, robotName);
        message.put(ORIENTATION, orientation);
        message.put(DISTANCE, distance);

        return message;
    }
 */
