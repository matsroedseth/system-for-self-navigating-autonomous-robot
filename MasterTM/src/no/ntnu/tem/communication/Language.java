/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.communication;

/**
 *
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public interface Language {
    
    // Naming convention for message content
    public final static String ROBOTNAME = "r";
    public final static String POSITION = "pd";
    public final static String IRDATA= "ird";
    public final static String IRHEADING = "irh";
    public final static int NUMBEROFSENSORS = 4; // det her bør vel ligge i handshaken?
    public final static String IRSENSOR = "s";
    public final static String POSITIONCOORDINATES = "p";
    public final static String TOWEROFFSET = "to";
    public final static int CHECKSUM = 15;
    public final static String IRSENSOROFFSET = "so";
    public final static String DISTANCE = "d";
    
    
    
    public final static int UPDATE = 1;
    public final static int HANDSHAKE = 2;
    
    public final static int MESSAGETYPE = 0;
    
    public final static int UPDATE_XPOS = 1;
    public final static int UPDATE_YPOS = 2;
    public final static int UPDATE_THETA = 3;
    public final static int UPDATE_TOWER_HEADING = 4;
    public final static int UPDATE_SENSOR_START = 5;
    public final static int UPDATE_SENSOR_END = 8;
    
    public final static int HANDSHAKE_WIDTH=1;
    public final static int HANDSHAKE_LENGTH=2;
    public final static int HANDSHAKE_TOWEROFFSET_Y=3;
    public final static int HANDSHAKE_TOWEROFFSET_X=4;
    public final static int HANDSHAKE_AXLEOFFSET = 5;
    public final static int HANDSHAKE_SENSOROFFSET_START = 6;
    public final static int HANDSHAKE_SENSOROFFSET_END = 9;
    public final static int HANDSHAKE_IRHEADING_START = 10;
    public final static int HANDSHAKE_IRHEADING_END = 13;
    public final static int HANDSHAKE_MESSAGEDEADLINE = 14;
    
//    public final static String ROBOTNAME = "r";
//    public final static String POSITION = "pd";
//    public final static String IRDATA= "ird";
//    public final static String IRHEADING = "irh";
//    public final static String ORIENTATION = "o";
//    public final static String MESSAGETYPE = "m";
//    public final static String IRSENSOR = "s";
//    public final static String POSITIONCOORDINATES = "p";
//    public final static String ROBOTWIDTH = "w";
//    public final static String ROBOTLENGTH = "l";
//    public final static String TOWEROFFSET = "to";
//    public final static String MESSAGEDEADLINE = "dl";
//    public final static String AXLEOFFSET = "ao";
//    public final static String IRSENSOROFFSET = "so";
//    public final static String DISTANCE = "d";
    
}
