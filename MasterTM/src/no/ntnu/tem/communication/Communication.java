/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.communication;

import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.UnsupportedCommOperationException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import no.ntnu.tem.application.RobotController;

/**
 *
 * @author matsroedseth
 */
public class Communication {

    private final Inbox inbox;
    private final InboxReader inR;
    private final SerialCommunication com;

    /**
     * Constructor of the class Communication
     *
     * @param rc the RobotController object
     */
    public Communication(RobotController rc) {
        this.inbox = new Inbox();
        this.inR = new InboxReader(inbox, rc);
        this.com = new SerialCommunication(inbox);
    }

    /**
     * Method for starting the communication module
     */
    public void startCommunication() {
        try {
            com.connect("COM6");
        } catch (UnsupportedCommOperationException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PortInUseException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPortException ex) {
            Logger.getLogger(Communication.class.getName()).log(Level.SEVERE, null, ex);
        }
        com.start();
    }

    public void startInboxReader() {
        inR.start();
    }

    /**
     * Method that returns the inbox object
     *
     * @return the inbox
     */
    public Inbox getInbox() {
        return this.inbox;
    }

    /**
     * Method that returns the serial communication object
     *
     * @return the serial communication
     */
    public SerialCommunication getSerialCommunication() {
        return com;
    }

    /**
     * Method that wraps a message and sends it to a robot
     *
     * @param robotName the receiving robot
     * @param orientation the robots wanted orientation
     * @param distance the distance to move
     */
    public void sendMessageToRobot(String robotName, double orientation, double distance) {
        com.send(MessageHandler.wrapMessage(robotName, orientation, distance));
    }
}
