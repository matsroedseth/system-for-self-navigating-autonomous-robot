/*
 * This code is written as a part of a Master Thesis
 * the spring of 2016.
 *
 * Thor Eivind Andersen and Mats Rødseth (Master 2016 @ NTNU)
 */
package no.ntnu.tem.communication;

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

/**
 *
 * @author Thor Eivind and Mats (Master 2016 @ NTNU)
 */
public class SerialCommunication extends Thread {

    private CommPortIdentifier portIdentifier;
    private CommPort commPort;
    private SerialPort serialPort;
    private InputStream inStream;
    private OutputStream outStream;
    private StringBuilder sb = new StringBuilder();
    private final Inbox inbox;
    private final static JSONParser PARSER = new JSONParser(JSONParser.MODE_JSON_SIMPLE);

    /**
     * Constructor of the class Communication
     *
     * @param inbox The systems message inbox
     */
    public SerialCommunication(Inbox inbox) {
        this.inbox = inbox;
    }

    /**
     * Method for checking if the robot is still connected to the system
     *
     * @return true if robot is still connected
     */
    public boolean ping() {
        return false;
    }

    /**
     * Method for connecting to a given port
     *
     * @param portName the port to connect to
     * @throws gnu.io.UnsupportedCommOperationException .
     * @throws gnu.io.PortInUseException .
     * @throws java.io.IOException .
     * @throws gnu.io.NoSuchPortException .
     */
    public void connect(String portName) throws UnsupportedCommOperationException,
            PortInUseException, IOException, NoSuchPortException {
        this.portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if (portIdentifier.isCurrentlyOwned()) {
            System.out.println("Error: Port is currently in use");
        } else {
            this.commPort = portIdentifier.open(this.getClass().getName(), 2000);

            if (commPort instanceof SerialPort) {
                this.serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(38400, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                serialPort.setDTR(true);
                serialPort.setRTS(true);

                this.inStream = serialPort.getInputStream();
                this.outStream = serialPort.getOutputStream();
            } else {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }
    }

    public void send(String message) {
        try {
            byte[] data = message.getBytes();
            outStream.write(data);
            outStream.write(10);
            outStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that scans for available robots
     */
    public void scan() {
        // scanne etter roboter
        //AT+SCAN+. 
    }

    @Deprecated
    /**
     * Method for receiving incoming messages and putting them in the inbox
     */
    private void receive() throws IOException {
        while (inStream.available() > 0) {
            // Get Response.

            try {
                JSONObject response;
                response = (JSONObject) PARSER.parse(inStream);

                inbox.putMessage(response);
                System.out.println("Inbox size: " + inbox.getInboxSize());
            } catch (ParseException ex) {
                Logger.getLogger(SerialCommunication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Ingenting å hente " + inStream.available());
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            Logger.getLogger(SerialCommunication.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Test method to receive messages from different robots
     */
    private void receiveMessages() {
        try {
            byte[] buffer = new byte[1024];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while (inStream.available() > 0) {
                bytesRead = inStream.read(buffer);
                output.write(buffer, 0, bytesRead);
                parseMessage(output.toByteArray());
                int i = 1;
            }
            output.close();
        } catch (IOException ex) {
            Logger.getLogger(SerialCommunication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Stores the incoming messages in a StringBuilder. When a message is
     * complete it will be putted in Inbox
     *
     * @param mess The new incoming message
     */
    private void parseMessage(byte[] messageAsByteArray) {
        sb.append(new String(messageAsByteArray));
        String message = sb.toString();
        String[] strings = message.split("\\[");
        for (int j = 0; j < strings.length; j++) {
            if (endOfMessage(strings[j])) {
                // Fått slutten på en melding, tøm buffer
                sb = new StringBuilder();
                for (int i = 1; i < strings.length; i++) {
                    if (i <= j) {
                        inbox.putMessage("[" + strings[i]);
                    } else {
                        sb.append("[" + strings[i]);
                    }
                }
            }
        }
    }

    private boolean endOfMessage(String mess) {
        if (mess.endsWith("\n")) {
            return true;
        } else {
            return false;
        }
    }

    @Deprecated
    /**
     * Test method that generates custom JSONObject-messages and puts them in
     * the inbox.
     */
    private void testReceive() {
        //Handshake
        JSONObject handshake = new JSONObject();                //HandshakeMessage
        handshake.put("r", "1");                          //RobotName
        handshake.put("m", 2);                                  //MessageType
        handshake.put("w", 10.0);                               //RobotWidth
        handshake.put("l", 20.0);                               //RobotLength
        handshake.put("to", new double[]{0.0, 0.0});            //TowerOffset
        handshake.put("ao", 0.0);                               //AxleOffset
        handshake.put("so", new double[]{0.0, 0.0, 0.0, 0.0});  //SensorOffset
        handshake.put("dl", 200);                               //MessageDeadline
        JSONObject irsensorheading = new JSONObject();
        irsensorheading.put("s1", 0);                           //IRsensorHeading
        irsensorheading.put("s2", 90);                          //IRsensorHeading
        irsensorheading.put("s3", 180);                         //IRsensorHeading
        irsensorheading.put("s4", 270);                         //IRsensorHeading
        handshake.put("irh", irsensorheading);
        inbox.putMessage(handshake);
        System.out.println("Handshake: " + handshake.toJSONString());

        try {
            Thread.sleep(500);
        } catch (InterruptedException ex) {
            Logger.getLogger(SerialCommunication.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (true) {
            //Update
            JSONObject update = new JSONObject();               //UpdateMessage

            JSONObject position = new JSONObject();
            position.put("p1", 0);                              //RobotXpos
            position.put("p2", 0);                              //RobotYpos

            JSONObject irsensordata = new JSONObject();
            irsensordata.put("s1", 10);                         //IRsensorData
            irsensordata.put("s2", 15);                         //IRsensorData
            irsensordata.put("s3", 10);                         //IRsensorData
            irsensordata.put("s4", 15);                         //IRsensorData

            irsensorheading = new JSONObject();
            irsensorheading.put("s1", 0);                       //IRsensorHeading
            irsensorheading.put("s2", 90);                      //IRsensorHeading
            irsensorheading.put("s3", 180);                     //IRsensorHeading
            irsensorheading.put("s4", 270);                     //IRsensorHeading

            update.put("r", "1");                               //RobotID
            update.put("m", 1);                                 //MessageType
            update.put("pd", position);                         //PositionData
            update.put("o", 0.0);                               //Orientation
            update.put("irh", irsensorheading);                 //IRHeading
            update.put("ird", irsensordata);                    //IRData
            inbox.putMessage(update);
            System.out.println("Update: " + update.toJSONString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SerialCommunication.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void run() {
        super.run();
        while (true) {
            receiveMessages();
        }
    }
}
